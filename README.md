## SOLICITAÇÃO DE PRAWO JAZDY (CNH POLONESA) EM KATOWICE 


## 1. Estudar para o exame:
* Simulado: https://drivingtest.portalnaukijazdy.pl/rejestracja
* Material: https://en.wikipedia.org/wiki/Road_signs_in_Poland
* custo: 25 PLN por 90 dias de acesso (existem opções mais baratas, mas recomendamos esta)
* idioma: inglês

>Está se sentindo confiante com o simulado? Pode dar inicio no processo.

## 2. Atualizar o endereço no sistema do governo polonês (zameldowanie):
* Documentação necessária: Levar Contrato de aluguel/escritura do imóvel caso seja o proprietário, documento polonês (VISA ou Karta Pobytu).
* endereço: `Urząd Miasta Katowice - Biuro Obsługi Mieszkańców`
* idioma: polonês, inglês
* custo: ±20PLN

## 3. Traduzir a CNH pra polonês: 

Conhecemos uma tradutora juramentada de Varsóvia.

* email: `aleksandrarutyna@o2.pl`
* custo: ±50PLN
* idioma: polonês, português

## 4. Iniciar o processo para solicitar a carteira de habilitação polonesa

Necessário estar com o endereço atualizado (zameldowanie) e a CNH traduzida para polonês

**IMPORTANTE**: Ao dar inicio no processo será gerado um `numero de protocolo`.

* endereço: `Urząd Miasta Katowice - Wydział Uprawnień Komunikacyjnych, Francuska 70, 40-028 Katowice`
* custo: ±100 PLN
* telefone: `+48 32 359 30 03`
* idioma: polonês

## 5. Agendar o teste teórico:
* documentação necessaria: Número de Protocolo e documento polonês (VISA ou Karta Pobytu).
* endereço: `WORD Katowice, Francuska 78, 40-507 Katowice`
* custo: 36 PLN por tentativa
* idioma: polonês (O pessoal só fala polonês mas o teste é em inglês).
* resultado: ao final da prova.

>Passou na prova? Parabéns, porém você ainda não está legalmente habilitado para dirigir na Polônia. 

>Seus dados pessoais e informações da prova serão encaminhados para embaixada para validação, e uma cópia da mesma carta será enviada para o seu endereço.

## 6. Embaixada do Brasil
Dada uma semana a partir do recebimento desta cópia, entrar em contato com a embaixada do Brasil em Varsóvia.

Solicite que verifiquem se o seu processo foi recebido, e eles irão te pedir que você emita um nada consta da sua CNH.

Isso pode ser feito através do sistema do Detran do seu estado. Cada estado pode ter um processo diferente, mas o importante é um documento que comprove a legitimidade e a ausência de pendências da sua CNH.

* Telefone: `+48 22 617 48 00`
* Plantão consular: `+48 608 094 328`
* Idioma: polonês e português
* e-mail: `consular.varsovia@itamaraty.gov.br`

## 7. Etapa final
Quando o processo é finalizado você será comunicado por SMS/e-mail sobre a data para retirar o documento no urząd komunikacji.

Qualquer dúvida que tenha sobre o processo após passar na prova, entrar em contato com urząd komunikacji.